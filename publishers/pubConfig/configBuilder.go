package pubConfig

type Configuration struct {
	URI          string
	ExchangeName string
	ExchangeType string
	RoutingKey   string
	ConsumerName string
	QueueName    string
	ConfigType   string
	ConfigName   string
}

func GetConfig(configName string) Configuration {

	c := Configuration{}
	if configName == "CRAWLER_SERVICE" {
		c.ConfigType = "RabbitMQ"
		c.ConfigName = "CRAWLER_SERVICE"
		c.URI = "amqp://test:test@3.86.69.81:5672"
		c.ExchangeName = "logs_topic"
		c.ExchangeType = "topic"
		c.RoutingKey = "test"
		c.ConsumerName = "Consumer Test"
		c.QueueName = "Queue 1"
	}

	return c

}
