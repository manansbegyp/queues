package publishers

import (
	"flag"
	"log"
	"time"

	"./pubConfig"
	"github.com/streadway/amqp"
)

/*
Publisher Struct
*/
type Publisher struct {
	conn   *amqp.Connection
	ch     *amqp.Channel
	config pubConfig.Configuration
}

/*
NewPublisher - Get new Publisher
*/
func NewPublisher(configName string) *Publisher {

	p := Publisher{}

	p.initAmqp(configName)

	return &p
}
func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)

	}
}

func (p *Publisher) initAmqp(configName string) {
	log.Printf("Getting Config for Publisher")
	p.config = pubConfig.GetConfig(configName)
	var err error
	amqpURI := flag.String("amqpPublisher", p.config.URI, "AMQP Publisher URI")

	p.conn, err = amqp.Dial(*amqpURI)
	failOnError(err, "Failed to connect to RabbitMQ")
	log.Printf("got Connection, getting Channel...")

	p.ch, err = p.conn.Channel()
	failOnError(err, "Failed to open a channel")
	log.Printf("got Channel, declaring Exchange (%s)", p.config.ExchangeName)

	err = p.ch.ExchangeDeclare(
		p.config.ExchangeName, // name
		p.config.ExchangeType, // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // noWait
		nil,                   // arguments
	)
	failOnError(err, "Failed to declare the Exchange")
	log.Printf("declared Exchange, declaring Queue (%s)", "")
}

/*
Publish - Publish the messages
*/
func (p *Publisher) Publish(body []byte) {

	var err error
	err = p.ch.Publish(
		p.config.ExchangeName, // exchange
		p.config.RoutingKey,   // routing key
		false,                 // mandatory
		false,                 // immediate
		amqp.Publishing{
			DeliveryMode: amqp.Transient,
			ContentType:  "application/json",
			Body:         []byte(body),
			Timestamp:    time.Now(),
		})
	log.Printf("Published message %s", string(body))

	failOnError(err, "Failed to Publish on RabbitMQ")
}

/*
Close the connection
*/
func (p Publisher) Close() {

	p.ch.Close()
	p.conn.Close()
	log.Printf("Closed Channel")
}
