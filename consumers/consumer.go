package consumers

import (
	"flag"

	"log"

	"./conConfig"
	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)

	}
}

/*
Consumer stuct
*/
type Consumer struct {
	conn      *amqp.Connection
	ch        *amqp.Channel
	responses <-chan amqp.Delivery
	config    conConfig.Configuration
}

/*
NewConsumer - Get new instance for the consumer
*/
func NewConsumer(configName string) *Consumer {

	c := Consumer{}

	c.initAmqp(configName)

	return &c

}

func (c *Consumer) initAmqp(configName string) {

	c.config = conConfig.GetConfig(configName)
	var err error
	var q amqp.Queue

	amqpURI := flag.String("amqpConsumer", c.config.URI, "AMQP Consumer URI")

	c.conn, err = amqp.Dial(*amqpURI)

	failOnError(err, "Failed to connect to RabbitMQ")

	log.Printf("got Connection, getting Channel...")

	c.ch, err = c.conn.Channel()
	failOnError(err, "Failed to open a channel")

	log.Printf("got Channel, declaring Exchange (%s)", c.config.ExchangeName)

	err = c.ch.ExchangeDeclare(
		c.config.ExchangeName, // name of the exchange
		c.config.ExchangeType, // type
		true,                  // durable
		false,                 // delete when complete
		false,                 // internal
		false,                 // noWait
		nil,                   // arguments
	)
	failOnError(err, "Failed to declare the Exchange")

	log.Printf("declared Exchange, declaring Queue (%s)", "")

	q, err = c.ch.QueueDeclare(
		c.config.QueueName, // name, leave empty to generate a unique name
		true,               // durable
		false,              // delete when usused
		false,              // exclusive
		false,              // noWait
		nil,                // arguments
	)
	failOnError(err, "Error declaring the Queue")

	log.Printf("declared Queue (%q %d messages, %d consumers), binding to Exchange (key %q)",
		q.Name, q.Messages, q.Consumers, c.config.BindingKey)

	err = c.ch.QueueBind(
		q.Name,                // name of the queue
		c.config.BindingKey,   // bindingKey
		c.config.ExchangeName, // sourceExchange
		false,                 // noWait
		nil,                   // arguments
	)
	failOnError(err, "Error binding to the Queue")

	log.Printf("Queue bound to Exchange, starting Consume (consumer tag %q)", c.config.ConsumerName)

	c.responses, err = c.ch.Consume(
		q.Name,                // queue
		c.config.ConsumerName, // consumer
		true,                  // auto-ack
		false,                 // exclusive
		false,                 // no-local
		false,                 // no-wait
		nil,                   // args
	)
	failOnError(err, "Error consuming the Queue")
}

/*
Close the channel and connection for a clean exit
*/
func (c *Consumer) Close() {

	c.ch.Close()
	c.conn.Close()
	log.Printf("Closed Channel")
}

/*
Consume - get  channel for consumption
*/
func (c *Consumer) Consume() <-chan amqp.Delivery {
	log.Printf("Starting Consumption")
	return c.responses
}
