This is the queue library for consuming and publishing message on queues

The library has three public methods:

1. New - Create a new instance of the publisher or consumer

2. Publish / Consume - Publish and consume messages

3. Close - Close the connection and channel


It is the library user's responsibility to call the correct instance, send the right byte package  and close the instance in case it needs to be abandoned. This gives enough flexibility for creation of instance and use.

ConfigBuilder builds the right configuration for the consumer or publisher and owns the configuration completely.

Right now, There is enough decoupling to migrate easily to a newer stack like kafka but its not enforced by an interface.

Plan is to migrate to a factory pattern when it is required. 

Refer main for examples