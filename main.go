package main

import (
	"fmt"
	"log"
	"time"

	"./consumers"
	"./publishers"
)

func main() {

	log.Println("Start consuming the Queue")

	c := consumers.NewConsumer("CRAWLER_SERVICE")

	go func() {
		for message := range c.Consume() {

			fmt.Printf(string(message.Body))
			time.Sleep(100000)

		}
	}()

	time.Sleep(10000)

	fmt.Printf("Start publishing on  the Queue")

	p := publishers.NewPublisher("CRAWLER_SERVICE")

	p.Publish([]byte("Test 1"))

	time.Sleep(10000)

	p.Publish([]byte("Test 2"))
	time.Sleep(10000)
	p.Publish([]byte("Test 3"))
	time.Sleep(10000)
	p.Close()
	c.Close()

}
